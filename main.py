import numpy as np
from sklearn.linear_model import LinearRegression
from datetime import datetime, date, timedelta
import requests

def todays_data(dataset = "fullyVaccPersons"):
    # Download list of all latest files available for download
    files_url = "https://www.covid19.admin.ch/api/data/context"
    files_resp = requests.get(files_url)
    files_json = files_resp.json()

    # Download the JSON stats for fully vaccinated people
    fully_vacc_url = files_json["sources"]["individual"]["json"][dataset]
    fully_vacc_resp = requests.get(fully_vacc_url)
    return fully_vacc_resp.json(), fully_vacc_url

def timestamp_for_json(o):
    return datetime.strptime(o["date"], '%Y-%m-%d').timestamp()

def dataset_for_regression(ch_entries, filter):
    timestamps = np.array([timestamp_for_json(o) for o in ch_entries if filter(o)]).reshape(-1, 1)
    totals = np.array([o["sumTotal"] for o in ch_entries if filter(o)])
    return timestamps, totals

def regression_in_interval(json_data, begin, end):
    filter = lambda o : timestamp_for_json(o) > begin and timestamp_for_json(o) < end
    return regression(json_data, filter)

def predict_date(population, model):
    result = (population - model.intercept_) / model.coef_
    return datetime.fromtimestamp(result[0])

def regression(json_data, filter = lambda _ : True):
    # Filter JSON data for the whole country
    # Schema docs:
    # https://www.covid19.admin.ch/api/data/documentation/models/sources-definitions-vaccinationincomingdata.md
    ch_entries = [o for o in json_data if o["geoRegion"] == "CH"]
    population = ch_entries[0]["pop"]

    # Linear regression, adapted from
    # https://realpython.com/linear-regression-in-python/
    timestamps, totals = dataset_for_regression(ch_entries, filter)
    model = LinearRegression()
    model.fit(timestamps, totals)
    correlation = model.score(timestamps, totals)

    # Predict the date where everyone will be vaccinated at the current rate
    quarter_vaccinated_date = predict_date(3 * population / 4, model)
    three_million_vacc_date = predict_date(3_000_000, model)

    # The model.coef_ is people per timestamp, we want a rate of people per day,
    # hence we multiply by the number of timestamps per day
    return { "75%": quarter_vaccinated_date,
             "3M": three_million_vacc_date,
             "corr": correlation,
             "speed": model.coef_[0] * 86400 }

def format_date(date):
    return date.strftime("%a %Y-%m-%d")

def print_results(title, data):
    print(title)
    print(f'  75% of Switzerland vaccinated on: {format_date(data["75%"])}')
    print(f'  3 million vaccinated on: {format_date(data["3M"])}')
    print(f'  Coefficient of correlation: {data["corr"]:.3f}')
    print(f'  Speed: {data["speed"]:.2f} people per day')

def main():
    today = datetime.today()
    previous_speed = -1
    data, file_url = todays_data()
    print('---')
    print(format_date(today))
    print(f'File: {file_url}')
    for week in reversed(range(1, 10)):
        begin = today - timedelta(weeks=week)
        end = today - timedelta(weeks=(week - 1))
        regr = regression_in_interval(data, begin.timestamp(), end.timestamp())
        print_results(f'WEEK FROM {format_date(begin)} TO {format_date(end)}:', regr)
        if previous_speed != -1:
            acceleration = (regr["speed"] - previous_speed) / previous_speed * 100
            print(f'  Acceleration: {acceleration:.2f}%')
        previous_speed = regr["speed"]

if __name__ == "__main__":
    main()
